import React from 'react';
import ReactDOM from 'react-dom';

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(), user: {
                firstName: 'Sreethin',
                lastName: 'Babu'
            }
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }
    formatName(user) {
        return user.firstName + ' ' + user.lastName;
    }


    render() {
        return (
            <div>

                <h1>
                    Hello, {this.formatName(this.state.user)}!
                </h1>

                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}

// ReactDOM.render(
//     <Clock />,
//     document.getElementById('root')
// );
export default Clock;